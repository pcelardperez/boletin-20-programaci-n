
import java.util.ArrayList;


/**
 *
 * @author hissarlid
 */
public class Correo {
    private String contenido;
    private boolean leido;
    
    
    public Correo(){
        
    }

    public Correo(String contenido, boolean leido){
        this.contenido=contenido;
        this.leido=leido;
    }
    /**
     * @return the contenido
     */
    public String getContenido() {
        return contenido;
    }

    /**
     * @param contenido the contenido to set
     */
    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    /**
     * @return the leido
     */
    public boolean isLeido() {
        return leido;
    }

    /**
     * @param leido the leido to set
     */
    public void setLeido(boolean leido) {
        this.leido = leido;
    }
    
    @Override
    public String toString(){
        if(this.isLeido()==true){
        String res = (" Correo: " + contenido + " \tLeido: SI");//podo por a variable directamente ou con this.get..
        return res;
        }else{
        String res = (" Correo: " + contenido + " \tLeido: NO");//podo por a variable directamente ou con this.get..
        return res;
        }
    }
    
    
    public void ArrayList(){
        
        ArrayList<Correo>Emails = new ArrayList<Correo>();
        
    }    
}
