
import java.util.ArrayList;
import javax.swing.JOptionPane;

/*
2- Define a clase Buzon para xestionar unbuzon de correo  electrónicos ordenados segundo a orde de chegada.
Para representar o buzon de correo úsase un array de correos electrónicos; estes, á súa vez, son obxectos da clase Correo e teñen como atributos un String, co contido do correo, 
e un indicativo para saber se foron lidos ou non.
Define a clase Correo cos métodos que creas convenientes, sabendo que a clase Buzon debe incluír os seguintes métodos públicos:
a) int numeroDeCorreos(), que calcula cantos correos hai no buzon de correo
b) void engade (Correo c), que engade c ao buzon
c) boolean porLer(), que determina se quedan correos por ler
d) String amosaPrimerNoLeido(), que amostra o primeiro correo non lido
e) String amosa(int k), que amostra o correo k-ésimo, fora lido ou non
f) void elimina(int k), que elimina o correo k-ésimo.
*/

/**
 *
 * @author hissarlid
 */
public class Aplicacion {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
        Buzon obx = new Buzon();
        ArrayList<Correo> Emails = new ArrayList<Correo>();
        
        Emails.add(new Correo("!Hola que tal", true));
        Emails.add(new Correo("SPAM!", false));
        Emails.add(new Correo("Te vendemos cosas", true));
        Emails.add(new Correo("Fotos trabajo", false));
        
        int opcion2=0;
        do{
        int opcion= Integer.parseInt(JOptionPane.showInputDialog("Que quieres hacer?:"
                + "\n1-Introducir correos"
                + "\n2-Ver numero de correos por leer"
                + "\n3-Ver todos los correos"
                + "\n4-Muestra el primer correo no leido"
                + "\n5-Buscar correo por número"
                + "\n6-Borrar un correo"));
        
        switch(opcion){
            case 1:
                obx.engade(Emails); //Añado correos
                break;
            case 2:
                obx.porLer(Emails); //Veo si hay correos por leer
                break;
            case 3:
                obx.verCorreos(Emails); //Veo todos los correos
                break;
            case 4:     /* Ver el primer correo no leido */
                JOptionPane.showMessageDialog(null,obx.amosaPrimerNoLeido(Emails)); //Ver el primero no leido
                break;
            case 5:     /* Buscar correo pasandole posición */
                int ncorreo=0;
                ncorreo=Integer.parseInt(JOptionPane.showInputDialog("Introduce el número del correo que quieres buscar"));
                JOptionPane.showMessageDialog(null,obx.amosa(ncorreo, Emails));
                /*JOptionPane.showMessageDialog(null,obx.amosa(ncorreo, Emails));*/ /*Este es un metodo más facil*/
                break;
            case 6:     /* Borrar un correo indicando su posición */
                int ncorreoaborrar;
                ncorreoaborrar=Integer.parseInt(JOptionPane.showInputDialog("Introduce el número del correo que quieres borrar"));
                int respuesta=JOptionPane.showConfirmDialog(null,"Seguro que quieres borrar el correo nº"+ncorreoaborrar/*+
                        JOptionPane.YES_NO_OPTION*/);
                        if(respuesta==JOptionPane.YES_OPTION){
                            obx.elimina(ncorreoaborrar, Emails);
                        }else if(respuesta==JOptionPane.NO_OPTION){
                            JOptionPane.showMessageDialog(null,"Correo NO borrado");
                        }else if(respuesta==JOptionPane.CLOSED_OPTION){
                            JOptionPane.showMessageDialog(null,"Operación cancelada");
                        }
                
                break;
            
        }
        opcion2= Integer.parseInt(JOptionPane.showInputDialog("Pulsa 0 para elegir otra opción o cualquier otra tecla para salir"));
        }while (opcion2==0);
        
    }
    
}
