
import java.util.ArrayList;
import javax.swing.JOptionPane;


/**
 *
 * @author hissarlid
 */
public class Buzon {
    Correo obx = new Correo();
    
    //esto tiene que ser void y devolverá el número de correos.
    public void numeroCorreos(){
       
    }
    
    //Añade c al buzon
    public void engade(ArrayList<Correo>Emails){
       String contenido;
       int auxleido;
       String continuar="0";
       boolean leido;
       
       do{
       contenido = JOptionPane.showInputDialog("Introduce el mensaje del e-mail: "+(Emails.size()+1));
        do{
            auxleido = Integer.parseInt(JOptionPane.showInputDialog("El e-mail número "+(Emails.size()+1)+" ha sido leido?\nPulsa 0 para NO o 1 para SI"));
            if(auxleido<0||auxleido>1){
                JOptionPane.showMessageDialog(null,"El valor debe ser 0 para NO LEIDO o 1 para LEIDO");
            }
            if(auxleido==0){
                leido=false;
            }else{
                leido=true;
            }
            
            Emails.add(new Correo(contenido, leido));
            
        }while(auxleido<0||auxleido>1);
            continuar = (JOptionPane.showInputDialog("Pulsa cualquier tecla para Continuar o 0 para Salir"));
       }while (continuar.compareTo("0") != 0);
    }
    
    
            
    //Esto tiene que ser boolean y retorna si hay o no correos por abrir
    public void porLer(ArrayList<Correo>Emails){
        int contador=0;
        for(Correo o :Emails){
            if(o.isLeido()==false){
                 contador=contador+1;
            }
            }
        if(contador>=1){
        JOptionPane.showMessageDialog(null, "Quedan "+contador+" correos por leer.");
        }else{
        JOptionPane.showMessageDialog(null,"No hay correos por leer");
        }
    }
    
    //Muestra todos los correos 
    public void verCorreos(ArrayList<Correo>Emails){
        for(Correo o:Emails){
            JOptionPane.showMessageDialog(null, o);
        }
    }
    
    //Tiene que ser String Enseña el primer correo no leido
    public String amosaPrimerNoLeido(ArrayList<Correo>Emails){
        String vcontido=null;
        for (Correo o:Emails){
            if(o.isLeido()==false){
                vcontido = o.getContenido(); 
                break;
            }
        }
        return vcontido;
    }
    
    //Tiene que ser String y muestra el correo en la posicion k, fuera leido o no 
    public String amosa(int k, ArrayList<Correo>Emails){
        
        String vcontido=null;
        int vpos=0;
        for(Correo o:Emails){
            vpos=(Emails.indexOf(o)+1);
            if(vpos==k){
                vcontido = o.getContenido();
            }
        }
        return vcontido;
        
        /*return Emails.get(k-1).getContenido();*/ /* Este es unmetodo mas facil de hacerlo */
    }
    
    //Elimina el contenido de k
    public void elimina(int k, ArrayList<Correo>Emails){
        
        
        Emails.remove(k-1);
        JOptionPane.showMessageDialog(null,"Correo borrado");
    }
}
